<?php include('header.php');?>

	<div id="template-wrapper" class="wrapper">
		<div id="template" class="site-content">

	
<section id="home-feature-wrapper" class="wrapper viewport-height">
	<div id="home-feature">
  	<div class="header_img viewport-height">
			<img 
				src="wp-content/themes/murmuration/images/home_murmuration.png" 
				class="tilt-effect" 
				data-tilt-options='{ "extraImgs" : 4, "opacity" : 1, "movement": { "perspective" : 500, "translateX" : -15, "translateY" : -15, "translateZ" : 20, "rotateX" : 15, "rotateY" : 15 } }' 
				alt="grid01" />
		</div>
  </div> <!-- #home-feature -->
    <div id="animated-headline">
	  
		<div id="word-1" class="ah-text yellow">THE</div>
		<div id="word-2" class="ah-text yellow">BEST</div>
	<!--	<div id="word-3" class="ah-text yellow">OF</div>-->
	<div id="word-4" class="ah-text white" style="left:10%;">INTERNSHIP</div>
		<div id="word-5" class="ah-text white">FEST</div>
		<div id="word-6" class="ah-text white">OF THE</div>
	<!--	<div id="word-7" class="ah-text yellow">&</div>-->
		<div id="word-8" class="ah-text white">WORLD<span>.</span></div>
		
	</div><!-- animated-headline -->
	
	
	<div id="feature-detail-cta">
		<div id="feature-detail">
			<div id="feature-date">
				DECEMBER 15 - 25, 2019
			</div>
			<div id="feature-location">
				GOA, INDIA
			</div>
		</div> <!-- #feature-detail -->
					<div id="feature-cta">
				<a href="tickets/index.php" class="button arrow-button">Winter 2019</a>
			</div><!-- #feature-cta -->
			</div><!-- #feature-detail-cta -->
	
</section><!-- #home-feature-wrapper -->


<script src="wp-content/themes/murmuration/js/tiltfx.js"></script>
	

<section id="home-fold" class="wrapper">
	<div id="home-fold-container" class="container" style="height:76px;
    padding:2%;">
	<!--	<div id="home-fold-twitter-feed" class="twitter-feed">
			
	<h6 class="twitter-handle">@internshipfest</h6>
	<div id="twitter-feed"></div>	
	<script src="wp-content/themes/murmuration/js/twitter-fetch.js"></script>
	<script>
		//Twitter Feed Init 
		var config1 = {
	  "id": '732996430957142016',
	  "domId": 'twitter-feed',
	  "maxTweets": 1,
	  "enableLinks": true,
	  "showPermalinks": false
		};
		twitterFetcher.fetch(config1);
	</script>
		</div>
		<div id="home-fold-email-signup" class="email-signup">
			





<a href="javascript:;" class="email-signup-trigger">
	<i class="fa fa-angle-down" aria-hidden="true"></i>
	<i class="fa fa-angle-up" aria-hidden="true"></i>
	<span>GET EMAIL UPDATES</span>
</a>
<div class="email-signup-content">
	<div class="email-signup-description">
		Be the first to receive lineup announcements, ticket info, and more.
	</div>
	
                <div class='gf_browser_chrome gform_wrapper' id='gform_wrapper_2' ><form method='post' enctype='multipart/form-data' target='gform_ajax_frame_2' id='gform_2'  action='http://murmurationfest.com/'>
                        <div class='gform_body'><ul id='gform_fields_2' class='gform_fields top_label form_sublabel_below description_below'><li id='field_2_1' class='gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible' ><label class='gfield_label' for='input_2_1' >Email<span class='gfield_required'>*</span></label><div class='ginput_container ginput_container_email'>
                            <input name='input_1' id='input_2_1' type='text' value='' class='medium' tabindex='1'   placeholder='ENTER EMAIL ADDRESS' aria-required="true" aria-invalid="false"/>
                        </div></li>
                            </ul></div>
        <div class='gform_footer top_label'> <input type='submit' id='gform_submit_button_2' class='gform_button button' value='Submit' tabindex='2' onclick='if(window["gf_submitting_2"]){return false;}  window["gf_submitting_2"]=true;  ' onkeypress='if( event.keyCode == 13 ){ if(window["gf_submitting_2"]){return false;} window["gf_submitting_2"]=true;  jQuery("#gform_2").trigger("submit",[true]); }' /> <input type='hidden' name='gform_ajax' value='form_id=2&amp;title=&amp;description=&amp;tabindex=1' />
            <input type='hidden' class='gform_hidden' name='is_submit_2' value='1' />
            <input type='hidden' class='gform_hidden' name='gform_submit' value='2' />
            
            <input type='hidden' class='gform_hidden' name='gform_unique_id' value='' />
            <input type='hidden' class='gform_hidden' name='state_2' value='WyJbXSIsIjJhNmJmYmU1YjcwM2VmOTZjMzEwNmNmNTM5NzBhMWFiIl0=' />
            <input type='hidden' class='gform_hidden' name='gform_target_page_number_2' id='gform_target_page_number_2' value='0' />
            <input type='hidden' class='gform_hidden' name='gform_source_page_number_2' id='gform_source_page_number_2' value='1' />
            <input type='hidden' name='gform_field_values' value='' />
            
        </div>
                        </form>
                        </div>
                <iframe style='display:none;width:0px;height:0px;' src='about:blank' name='gform_ajax_frame_2' id='gform_ajax_frame_2'>This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe>
                <script type='text/javascript'>jQuery(document).ready(function($){gformInitSpinner( 2, 'wp-content/plugins/gravityforms/images/spinner.gif' );jQuery('#gform_ajax_frame_2').load( function(){var contents = jQuery(this).contents().find('*').html();var is_postback = contents.indexOf('GF_AJAX_POSTBACK') >= 0;if(!is_postback){return;}var form_content = jQuery(this).contents().find('#gform_wrapper_2');var is_confirmation = jQuery(this).contents().find('#gform_confirmation_wrapper_2').length > 0;var is_redirect = contents.indexOf('gformRedirect(){') >= 0;var is_form = form_content.length > 0 && ! is_redirect && ! is_confirmation;if(is_form){jQuery('#gform_wrapper_2').html(form_content.html());if(form_content.hasClass('gform_validation_error')){jQuery('#gform_wrapper_2').addClass('gform_validation_error');} else {jQuery('#gform_wrapper_2').removeClass('gform_validation_error');}setTimeout( function() { /* delay the scroll by 50 milliseconds to fix a bug in chrome */  }, 50 );if(window['gformInitDatepicker']) {gformInitDatepicker();}if(window['gformInitPriceFields']) {gformInitPriceFields();}var current_page = jQuery('#gform_source_page_number_2').val();gformInitSpinner( 2, 'wp-content/plugins/gravityforms/images/spinner.gif' );jQuery(document).trigger('gform_page_loaded', [2, current_page]);window['gf_submitting_2'] = false;}else if(!is_redirect){var confirmation_content = jQuery(this).contents().find('.GF_AJAX_POSTBACK').html();if(!confirmation_content){confirmation_content = contents;}setTimeout(function(){jQuery('#gform_wrapper_2').replaceWith(confirmation_content);jQuery(document).trigger('gform_confirmation_loaded', [2]);window['gf_submitting_2'] = false;}, 50);}else{jQuery('#gform_2').append(contents);if(window['gformRedirect']) {gformRedirect();}}jQuery(document).trigger('gform_post_render', [2, current_page]);} );} );</script><script type='text/javascript'> if(typeof gf_global == 'undefined') var gf_global = {"gf_currency_config":{"name":"U.S. Dollar","symbol_left":"$","symbol_right":"","symbol_padding":"","thousand_separator":",","decimal_separator":".","decimals":2},"base_url":"http:\/\/murmurationfest.com\/wp-content\/plugins\/gravityforms","number_formats":[],"spinnerUrl":"http:\/\/murmurationfest.com\/wp-content\/plugins\/gravityforms\/images\/spinner.gif"};jQuery(document).bind('gform_post_render', function(event, formId, currentPage){if(formId == 2) {if(typeof Placeholders != 'undefined'){
                        Placeholders.enable();
                    }} } );jQuery(document).bind('gform_post_conditional_logic', function(event, formId, fields, isInit){} );</script><script type='text/javascript'> jQuery(document).ready(function(){jQuery(document).trigger('gform_post_render', [2, 1]) } ); </script></div> 	</div><!-- #home-fold-email-signup -->
		<marquee onmouseover="this.stop();" onmouseout="this.start();" scrollamount="10">
		    
		   	 <p style='color:#eb2242;font-size:18px;font-weight:bold;text-transform:uppercase;word-spacing:4px;'>
		   	      <a href="tickets" id="noti">Registration for Summer Internship Fest 2020 shall come in soon.
				<!--<a href="tickets" class="button arrow-button">Summer 2020 (Pre Booking)</a>-->
			</p></marquee>
	</div><!-- #home-fold-container -->
</section><!-- #home-fold -->


	
	<div id="home-welcome-message" class="wrapper">
		<div id="home-welcome-message-container" class="container">
							<div class="column four offset-seven"> 
					<h3>WHAT IS<br>INTERNSHIP FEST?<br></h3>
				</div>
							<div class="column six offset-ten"> 
					The internship festival is an opportunity to learn from industry experts. The eleven day program is placed at the intersection of knowledge, opportunity and recreation. We invite curious minds and adventurous souls to join us as we embark on this fun-filled career enriching experience.
<br /> <br />
<a
style="
color: #f4ee6f;
margin: 0 0.25em;
border: 1px solid;
line-height: 2.5em;
padding: 0.925em 1.25em;
border-radius: 3px;
letter-spacing: 1px;"

   href="sponsors/index.html">VIEW OUR SPONSORS</a>				</div>
					</div><!-- #home-welcome-message-container -->	
	</div><!-- #home-welcome-message -->
	
	<div id="home-event-slider" class="wrapper">
	    	<h3 style="text-align:center;color:#f4ee6f;font-size:1.75em;text-transform: uppercase;text-decoration:underline;">Training Programs<br></h3>
		<div class="music_thought_title container">
			<h1 class="music"><a href="event-category/tech/index.php"> TECH</a></h1>
			<h1 class="thought"><a href="event-category/mobility/index.php">MOBILITY</a></h1>
							<div id="feature-cta">
					<a href="tickets/index.php" class="button arrow-button">Winter 2019</a>
				</div><!-- #feature-cta -->
					</div> <!-- .music_thought_title -->
		
					<div class="event-slider">
									<div class="event-slide music">
						 	<a class="event-slide-thumb" href="devops" target="_blank">
									
									<img src="wp-content/uploads/2016/05/devops.jpg" />
																<div class="event-details">
									<div class="event-name">
											<h2><span>Dev OPS</span></h2>
																					
																			</div>
								</div>
							</a>
						 
						 
					</div>
									<div class="event-slide thought">
							<a class="event-slide-thumb" href="electric-vehicle" target="_blank">
																	<img width="500" height="400" src="wp-content/uploads/2016/05/carsharing-4382651_1920.jpg" class="attachment-event_slide_thumb size-event_slide_thumb wp-post-image" alt="" />																<div class="event-details">
									<div class="event-name">
											<h2><span>Electric Vehicle</span></h2>
																			</div>
								</div>
							</a>
					</div>
									
								
								
								<!--	<div class="event-slide thought">
							<a class="event-slide-thumb" href="lineup/technology-of-storytelling/index.html">
									
									<img src="wp-content/themes/murmuration/images/trans-bg-500x400.png" />
																<div class="event-details">
									<div class="event-name">
											<h2><span>The Art of Science: A Look Inside the Technology of Storytelling</span></h2>
																					<h3><span>Talk</span></h3>
																			</div>
								</div>
							</a>
					</div>
									<div class="event-slide music">
							<a class="event-slide-thumb" href="lineup/yacht/index.html">
																	<img width="500" height="400" src="wp-content/uploads/2016/04/yacht-500x400.jpg" class="attachment-event_slide_thumb size-event_slide_thumb wp-post-image" alt="" />																<div class="event-details">
									<div class="event-name">
											<h2><span>Yacht</span></h2>
																			</div>
								</div>
							</a>
					</div>
									<div class="event-slide thought">
							<a class="event-slide-thumb" href="lineup/mark-hatch/index.html">
									
									<img src="wp-content/themes/murmuration/images/trans-bg-500x400.png" />
																<div class="event-details">
									<div class="event-name">
											<h2><span>Boom! Welcome to the Maker Revolution.</span></h2>
																					<h3><span>Talk</span></h3>
																			</div>
								</div>
							</a>
					</div>
									<div class="event-slide music">
							<a class="event-slide-thumb" href="lineup/dan-deacon/index.html">
																	<img width="500" height="400" src="wp-content/uploads/2016/05/dandeacon-500x400.jpg" class="attachment-event_slide_thumb size-event_slide_thumb wp-post-image" alt="" />																<div class="event-details">
									<div class="event-name">
											<h2><span>Dan Deacon</span></h2>
																			</div>
								</div>
							</a>
					</div> -->
									
							</div><!-- .event-slider -->
			<div class="home-event-cta container">
				<a class="button red-button" href="event-category/tech/index.php">Tech</a>
				<a class="button yellow-button" href="event-category/mobility/index.php">Mobility</a>
			</div>  <!-- .home-event-cta -->
			</div> <!-- #home-event-slider -->	
			
	<div id="home-event-slider" class="wrapper">
	    	<h3 style="text-align:center;color:#f4ee6f;font-size:1.75em;text-transform: uppercase;text-decoration:underline;">Themed Events<br></h3>
		<div class="music_thought_title container">
			<h1 class="music"><a href="event-category/guest-lectures">ATTRACTIONS  </a></h1>
			<h1 class="thought"><a href="attractions">GUEST LECTURE</a></h1>
							<div id="feature-cta">
					<a href="tickets/index.php" class="button arrow-button">Winter 2019</a>
				</div><!-- #feature-cta -->
					</div> <!-- .music_thought_title -->
		
					<div class="event-slider">
									<div class="event-slide music">
						 	<a class="event-slide-thumb" href="devops" target="_blank">
									
									<img src="wp-content/uploads/2016/05/devops.jpg" />
																<div class="event-details">
									<div class="event-name">
											<h2><span>Dev OPS</span></h2>
																					
																			</div>
								</div>
							</a>
						 
						 
					</div>
									<div class="event-slide thought">
							<a class="event-slide-thumb" href="electric-vehicle" target="_blank">
																	<img width="500" height="400" src="wp-content/uploads/2016/05/carsharing-4382651_1920.jpg" class="attachment-event_slide_thumb size-event_slide_thumb wp-post-image" alt="" />																<div class="event-details">
									<div class="event-name">
											<h2><span>Electric Vehicle</span></h2>
																			</div>
								</div>
							</a>
					</div>
									
								
								
								<!--	<div class="event-slide thought">
							<a class="event-slide-thumb" href="lineup/technology-of-storytelling/index.html">
									
									<img src="wp-content/themes/murmuration/images/trans-bg-500x400.png" />
																<div class="event-details">
									<div class="event-name">
											<h2><span>The Art of Science: A Look Inside the Technology of Storytelling</span></h2>
																					<h3><span>Talk</span></h3>
																			</div>
								</div>
							</a>
					</div>
									<div class="event-slide music">
							<a class="event-slide-thumb" href="lineup/yacht/index.html">
																	<img width="500" height="400" src="wp-content/uploads/2016/04/yacht-500x400.jpg" class="attachment-event_slide_thumb size-event_slide_thumb wp-post-image" alt="" />																<div class="event-details">
									<div class="event-name">
											<h2><span>Yacht</span></h2>
																			</div>
								</div>
							</a>
					</div>
									<div class="event-slide thought">
							<a class="event-slide-thumb" href="lineup/mark-hatch/index.html">
									
									<img src="wp-content/themes/murmuration/images/trans-bg-500x400.png" />
																<div class="event-details">
									<div class="event-name">
											<h2><span>Boom! Welcome to the Maker Revolution.</span></h2>
																					<h3><span>Talk</span></h3>
																			</div>
								</div>
							</a>
					</div>
									<div class="event-slide music">
							<a class="event-slide-thumb" href="lineup/dan-deacon/index.html">
																	<img width="500" height="400" src="wp-content/uploads/2016/05/dandeacon-500x400.jpg" class="attachment-event_slide_thumb size-event_slide_thumb wp-post-image" alt="" />																<div class="event-details">
									<div class="event-name">
											<h2><span>Dan Deacon</span></h2>
																			</div>
								</div>
							</a>
					</div> -->
									
							</div><!-- .event-slider -->
			<div class="home-event-cta container">
				<a class="button red-button" href="event-category/guest-lectures" style="width: 13%;">Guest Lectures</a>
				<a class="button yellow-button" href="attractions">Attractions</a>
			</div>  <!-- .home-event-cta -->
			</div> <!-- #home-event-slider -->
	


<div class="container">
 	<div>
			<span class="music" style="background:#eb2242;padding:28px;font-size: 24px;font-family:'Fabrikat-Bold', sans-serif"> OUR SPONSORS</span>
		<!-- #feature-cta -->
					</div> <!-- .music_thought_title -->
<br><br><br>
  <div class="row" style="background:white;text-align:center;">
      <div class="col-sm-12" style="text-align:center;">
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      </div>
      <div class="col-sm-12">
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      <div class="col-sm-1"><img src="asset/sponsors/hundai.png"></div>
      </div>
  </div>
  </div>
	 <br><br>
	<script>
		$('.event-slider').slick({
		  infinite: false,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  appendArrows: '.home-event-cta.container',
		  responsive: [
		    {
		      breakpoint: 1400,
		      settings: {
						infinite: false,
		        slidesToShow: 4,
		      }
		    },
		    {
		      breakpoint: 959,
		      settings: {
			      infinite: false,
		        slidesToShow: 3,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
			      infinite: false,
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 580,
		      settings: {
			      infinite: false,
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	</script>
	

	
	

	

		</div><!-- #template -->
	</div><!-- #template-wrapper -->
<?php include('footer.php');?>